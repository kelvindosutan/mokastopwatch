package kelvindosutan.mokastopwatch;

/**
 * Created by Kelvindo Sutan on 05/10/2017.
 */

public class History {

    private String number, date, result;

    public History(){

    }

    public History(String date, String result){
        this.date = date;
        this.result = result;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
