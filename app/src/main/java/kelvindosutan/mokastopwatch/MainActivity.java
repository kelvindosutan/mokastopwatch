package kelvindosutan.mokastopwatch;

import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private TextView txtView;
    private RecyclerView recyclerView;
    private HistoryAdapter hAdapter;
    private Button btnTrigger;

    long startTime, miliTime;
    int miliSeconds, seconds, minutes, hours;

    private List<History> historyList = new ArrayList<>();

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtView = (TextView) findViewById(R.id.textView);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        btnTrigger = (Button) findViewById(R.id.btnTrigger);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtView.setText(Html.fromHtml("00:00:00.<small>000</small>", Html.FROM_HTML_MODE_LEGACY));
        } else {
            txtView.setText(Html.fromHtml("00:00:00.<small>000</small>"));
        }

        handler = new Handler();
        final DatabaseHandler db = new DatabaseHandler(this);
        historyList = db.getAllHistory();
        hAdapter = new HistoryAdapter(historyList);
        RecyclerView.LayoutManager hLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(hLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(hAdapter);

        btnTrigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnTrigger.getText().toString().equals("START")) {
                    startTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                    btnTrigger.setText("STOP");
                } else {
                    Date currentDate = Calendar.getInstance().getTime();
                    SimpleDateFormat dateOnly = new SimpleDateFormat("dd MMM yyyy");
                    History history = new History(dateOnly.format(currentDate),
                            txtView.getText().toString());
                    db.addHistory(history);
                    historyList.add(history);
                    handler.removeCallbacks(runnable);
                    hAdapter.notifyDataSetChanged();
                    btnTrigger.setText("START");
                }
            }
        });

    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            miliTime = SystemClock.uptimeMillis() - startTime;

            seconds = (int) (miliTime / 1000);
            miliSeconds = (int) (miliTime % 1000);
            minutes = seconds / 60;
            seconds = seconds % 60;
            hours = minutes / 60;
            minutes = minutes % 60;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtView.setText(Html.fromHtml("" + String.format("%02d", hours) + ":"
                                + String.format("%02d", minutes)
                                + ":" + String.format("%02d", seconds) + ".<small>"
                                + String.format("%03d", miliSeconds) + "</small>",
                        Html.FROM_HTML_MODE_LEGACY));
            } else {

                txtView.setText(Html.fromHtml("" + String.format("%02d", hours) + ":"
                        + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds) + ".<small>"
                        + String.format("%03d", miliSeconds) + "</small>"));
            }

            handler.postDelayed(this, 0);
        }
    };
}
