package kelvindosutan.mokastopwatch;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Kelvindo Sutan on 05/10/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private List<History> historyList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView number, date, result;

        public MyViewHolder(View itemView) {
            super(itemView);
            number = (TextView) itemView.findViewById(R.id.number);
            date = (TextView) itemView.findViewById(R.id.date);
            result = (TextView) itemView.findViewById(R.id.result);
        }
    }

    public HistoryAdapter(List<History> historyList) {
        this.historyList = historyList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        History history = historyList.get(position);
        holder.number.setText("" + (position + 1));
        holder.date.setText(history.getDate());
        String fullResult = history.getResult();
        String miliSeconds = fullResult.substring(fullResult.indexOf(".") + 1);
//        holder.result.setText(history.getResult());
        String others = history.getResult().substring(0, fullResult.indexOf(".") + 1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.result.setText(Html.fromHtml(others + "<small>" + miliSeconds + "</small>",
                    Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.result.setText(Html.fromHtml(others + "<small>" + miliSeconds + "</small>"));
        }
    }


    @Override
    public int getItemCount() {
        return historyList.size();
    }


}
