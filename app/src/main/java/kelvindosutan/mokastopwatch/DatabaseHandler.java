package kelvindosutan.mokastopwatch;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelvindo Sutan on 06/10/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "mokaStopwatch";
    private static final String TABLE_HISTORY = "history";

    private static final String KEY_ID = "history_id";
    private static final String KEY_DATE = "history_date";
    private static final String KEY_RESULT = "history_result";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_HISTORY_TABLE = "CREATE TABLE "
                .concat(TABLE_HISTORY)
                .concat(" ( ")
                .concat(KEY_ID)
                .concat(" INTEGER PRIMARY KEY, ")
                .concat(KEY_DATE)
                .concat(" TEXT, ")
                .concat(KEY_RESULT)
                .concat(" TEXT ) ");
        db.execSQL(CREATE_HISTORY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        onCreate(db);
    }

    public void addHistory(History history) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, history.getDate());
        values.put(KEY_RESULT, history.getResult());

        db.insert(TABLE_HISTORY, null, values);
        db.close();
    }

    public List<History> getAllHistory() {
        List<History> historyList = new ArrayList<History>();

        String selectQuery = "SELECT * FROM "
                .concat(TABLE_HISTORY);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                History history = new History();
                history.setDate(cursor.getString(1));
                history.setResult(cursor.getString(2));
                historyList.add(history);
            } while (cursor.moveToNext());
        }

        return historyList;
    }

}
